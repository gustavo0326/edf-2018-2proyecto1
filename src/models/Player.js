class Player extends Element{

    constructor(src){
        super();
        this.initDOMElement();
        this._id = null;
        this._audio = null;
        this.initAudio(src);
    }

    set id(value){
        this._id = value;
        if(this.DOMElement != null){
            this.DOMElement.id = `player${this.id}`;
            this.DOMElement.dataset.id = `${this.id}`;
        }
    }
    

    get id(){
        return this._id;
    }

    set audio(value){
        this._audio = value;
    }

    get audio(){
        return this._audio;
    }

    initDOMElement(){
        var player = document.createElement("div");
        player.classList.add("player");

        var cover = document.createElement("div");
        cover.classList.add("cover");

        var img = document.createElement("img");
        img.src = "assets/covers/commingHome.jpeg";

        cover.appendChild(img);
        player.appendChild(cover);

        var timeline = document.createElement("div");
        timeline.classList.add("timeline");
        player.appendChild(timeline);

        var controls = document.createElement("div");
        controls.classList.add("controls","hiden");
        var title = document.createElement("div");
        title.classList.add("songTitle");
        title.innerHTML = "Comming Home";
        var playBtn = document.createElement("div");
        playBtn.classList.add("playBtn");
        var span = document.createElement("span");
        span.innerHTML = "&#9654;";
        playBtn.appendChild(span);
        controls.appendChild(title);
        controls.appendChild(playBtn);
        player.appendChild(controls);

        this.DOMElement = player;
        this.initEvents();
    }

    initEvents(){
        this.DOMElement.onmouseenter = () => {this.showControls(this.DOMElement);};
        this.DOMElement.onmouseleave = () => {this.hideControls(this.DOMElement);};
        this.DOMElement.onclick = () => {this.play();};
    }

    initAudio(src){
        this.audio = document.createElement("audio");
        this.audio.src = src;
        this.audio.ontimeupdate = () => {
            var timeline = this.DOMElement.querySelector(".timeline");
            var percent = (this.audio.currentTime / this.audio.duration) * 100;
            timeline.style.width = `${percent}%`;
        }
    }

    showControls(element){
        var controls = element.querySelector(".controls");
        controls.classList.remove("hiden");
        var img = element.querySelector("img");
        img.classList.add("blurred");
    }

    hideControls(element){
        if( PLAYING == this.id ){ return false; }
        var controls = element.querySelector(".controls");
        controls.classList.add("hiden");
        var img = element.querySelector("img");
        img.classList.remove("blurred");
    }

    play(){
        var icono = "";
        if( PLAYING != this.id && PLAYING != null ){
           players[PLAYING].play(); 
        }
        if(this.audio.paused){
            this.audio.play();
            icono = "&#9614;&#9614;";
            PLAYING = this.id;
        }else{
            this.audio.pause();
            icono = "&#9654;";
            PLAYING = null;
        }
        var span = this.DOMElement.querySelector(".controls .playBtn span");
        span.innerHTML = icono;
    }
  
  }